package com.automation;

public abstract class Figure {
    private int sideLength;

    public Figure (int sideLength){
        this.sideLength = sideLength;
    }

    public int getSideLength() {
        return sideLength;
    }

    public void setSideLength(int sideLength) {
        this.sideLength = sideLength;
    }

    public abstract double getPerimeter();
    public abstract double getArea();
}
