package com.automation;

public class Square extends Figure{

    public Square(int sideLength) {
        super(sideLength);
    }

    @Override
    public double getPerimeter() {
        return 4 * this.getSideLength();
    }

    @Override
    public double getArea() {
        return Math.pow(this.getSideLength(), 2);
    }
}
